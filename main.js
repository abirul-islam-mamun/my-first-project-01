
//if else condition:
let times = 20;

if (times < 14) {
    console.log("Good afternoon");
} else if (times < 18) {
    console.log("good Day");
} else {
    console.log("good Night");
}


//leap year:
function leapYear(year) {
    if ((year % 400 === 0) || (year % 100 != 0) && (year % 4 === 0) ) {
        console.log(`${year} is leap year`)
    } else {
        console.log(`${year} is not leap year`)
    }
}

leapYear (1900);

// var year = 1700;

// if (year % 4 === 0) {
//     console.log("leap year");
// } else if ((year % 100 != 0) && (year % 400 === 0)) {
//     console.log("leap year");
// } else {
//     console.log("not Leap year");
// }



//random number


// function ludu (min, max) {
//     return Math.floor(Math.random() * (max -min + 1) + min);
// }
// console.log(ludu(1, 6));

function ludu (min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


// for loop:
for (i = 0; i <= 20; i++) {
    console.log(i);
}

for (i = 0; i <= 20; i +=2) {
    console.log(i)
}

let man = ["Anik", "Partho", "Shaikat", "Ritu", "Joy"];

let manLength = man.length;

console.log(manLength)

console.log("  ")

for (i = 0; i < manLength; i++) {   //inex number print
    console.log(i);
}


// name print:
for (i = 0; i < manLength; i++) {
    console.log(man[i]); 
}

// even number 
console.log("even number:")
for (i = 2; i <= 20; i += 2) {
    console.log(i);
}

//odd number 
console.log("odd number:")
for (i = 1; i <= 20; i += 2) {
    console.log(i);
}

// for in loop  (ফর ইন লুপ অবজেক্ট এর ক্ষেত্রে ব্যবহারকরা হয়ে থাকে। যাতে আমরা প্রতিটা প্রপারটি এর মধ্য থেকে ইটারেট করতে পারি।)
// for in loop Array এর ক্ষেত্রেও ব্যবহারা করা যায়। but best option for array is for of loop.

const member = {member01: "Ani", member02: "Debo", member03: "Hasib"};

for (let x in member) {
    console.log(x);  //এখানে x দ্বারা property name denote করা হয়। 
}

const soMember = {member01: "Ani", member02: "Debo", member03: "Hasib"};

for (let x in soMember) {
    console.log(soMember[x]);
}


//for of loop (array, string এর মধ্যে দিয়ে লুপ করার জন্য।)
let stng = "Bangladesh";

for (let x of stng) {
    console.log(x);
}

let counter = [39, 33, 20, 23];

for (let x of counter) {
    console.log(x)
}



//Problem-01: search :
let words = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Temporibus amet, corrupti consequatur, sed sint recusandae reprehenderit ipsum cupiditate, ducimus accusamus quisquam! Aut quia quas eaque expedita voluptatum exercitationem dolor sit amar iusto ea?"

// let wordSearch = words.match(/dolor/ig);

// let count = wordSearch ? wordSearch.length : "there is not a word"; //(যদি wordSearch true হয় তাহলে wordSearch.length return হবে আর যদি false হয় তাহলে clone (:) এর পরের অংশ return করবে।)
// console.log(count);

// let wordPosition = words.search(/ipsum/i);

// let wordPositionCount = wordPosition >= 0 ? wordPosition : "there is no word like this";
// console.log(wordPositionCount)


let wordMatch = words.match(/sit/ig);
// let wordLength = wordMatch.length;                      (বুঝতে হবে)                       
let wordLength = wordMatch ? wordMatch.length : "eKhane kisu nei";
console.log(wordLength);


// problem-02:

// function lenier (arr, val) {
//     const length = arr.length
//     for (let i = 0; i < length; i++) {
//         if (arr[i] === val) {
//             return i;
//         }
//     }
//     return "not found";
// }


console.log(lenier (["a", "b", "c", "d"], "c"));

function lenier (array, value) {
    const length = array.length;
    for (let i = 0; i < length; i++) {
        if (array[i] === value) {
            return i;
        }
    }
    return "not found";

}


// কোনো array থেকে কিভাবে সবথেকে বড় string টি খুজে বের করবো এবং তার index number? /////


// function bigstring (array) {
//     let longword = "";
//     for (x of array) {
//         if (x.length > longword.length) {
//             longword = x;
//         }
//     }
//     return longword;
// }
function bigstring (array) {
    let longWord = "";
    for (let x of array ) {
        if (x.length > longWord.length) {
            longWord = x;
        }
    }

    return [longWord, array.indexOf(longWord)];
}
console.log(bigstring(["amar", "sonar", "bangladesh", "bet", "Ami tomay valobashi", "valobashi"]));



// 1 - 100 অব্দি কোন সংখ্যাগুলো ৩, ৫ দ্বারা বিভাজ্য এবং ৩ ও ৫  উভয় দ্বারা বিভাজ্য? /

// function threeFive (number) { 
//     for (let i = 1; i <= number; i++) {
//         if (i % 15 === 0) {
//             console.log(`${i} threeFive modulus`);
//         } else if (i % 3 === 0) {
//             console.log(`${i} is three modulus`);
//         } else if (i % 5 === 0) {
//             console.log(`${i} is five modulus`);
//         } else {
//             console.log(i)
//         }
//     }
// }
// threeFive(100);

function threeFive (number) {
    for (i = 1; i <= number; i++) {
        if (i % 15 === 0) {
            console.log(`${i} is a threeFive Modulus`);
        } else if (i % 5 === 0) {
            console.log(`${i} is is a Five Modulus`);
        } else if (i % 3 === 0) {
            console.log(`${i} is a Three modulus`);
        } else {
            console.log(i);
        }
    }
}

threeFive (50);


// কোনো অ্যারে থেকে Falsy value গুলো বের করে আনতে হবে। 

const myArray = ["java", "Script", false, "mamun", true, undefined, null, NaN, "", 0, "college"];

// const trueArray = myArray.filter (function(element) {
//     if (element) {
//         return true;
//     } else {
//         return false;
//     }
// })

const trueArray = myArray.filter(Boolean);
console.log(trueArray);


// কোনো Object থেকে Falsy value গুলো বের করে আনতে হবে।//////

let allObj = {
    a: "aim",
    b: false,
    c: undefined,
    d: "",
    e: null,
    f: NaN,
    g: 0,
    h: "thanks",
    i: "ami"
};

// let trueOb = function(allObj) {
//     for (let x in allObj) {
//         if (!allObj[x]) {
//             delete allObj [x];
//         }
//     }
//     return allObj;
// }
// console.log(trueOb(allObj));


let falsob = function (allObj) {
    for (let x in allObj) {
        if (allObj[x]){
            delete allObj [x];
        }
    }
    return allObj;
}
// let allans = falsob(allObj);
console.log(falsob(allObj))

let objlength = Object.keys(falsob(allObj)).length;
console.log(objlength);




